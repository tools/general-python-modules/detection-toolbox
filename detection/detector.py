import numpy as np
import scipy.signal as ss
from sklearn.cluster import MeanShift, get_bin_seeds


class Hitbox():
    def __init__(self, left, right, bottom, top):
        self.left = left
        self.right = right
        self.top = top
        self.bot = bottom

        self.hitCounter = 0
        self.leftCounter = 0

        self.toggledState = False

        self.wasHit = False
        self.minLeftCounter = 5

    def __within__(self, value, minimum, maximum):
        if value >= minimum and value <= maximum:
            return True
        else:
            return False

    def hit(self, x, y):
        if self.__within__(x, self.left, self.right) and self.__within__(y, self.bot, self.top):
            self.wasHit = True

    def evaluate(self):
        if self.wasHit and not self.toggledState:
            self.toggledState = True
            newHit = True

        else:
            newHit = False

        if not self.wasHit:
            self.leftCounter += 1

        else:
            self.leftCounter = 0

        if self.leftCounter >= self.minLeftCounter:
            self.toggledState = False

        self.wasHit = False

        return newHit


class DetectorSettings():
    def __init__(self):
        self.bw = 2
        self.binSize = 5
        self.minBinFreq = self.bw


class Centroid():
    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.distance = None

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @x.setter
    def x(self, new_x):
        self._x = new_x

    @y.setter
    def y(self, new_y):
        self._y = new_y

    @property
    def position(self):
        return (self.x, self.y)


class Detector():
    def __init__(self, settings=None):
        if settings is None:
            self.settings = DetectorSettings()
        else:
            self.settings = settings

        self.clusters = None
        self.centerPoints = None

    def run(self, points):
        if points is not None:
            return self.__findClusters__(points)

        else:
            return None

    def __findClusters__(self, points):
        self.centerPoints = None
        self.clusters = None

        # Prepare array shape
        pointArray = np.array(points)
        distances = pointArray[:2, :].transpose()

        if len(distances > 0):
            performClustering = True

        else:
            performClustering = False

        if performClustering:
            seeds = get_bin_seeds(distances, self.settings.binSize,
                                  self.settings.minBinFreq)

            try:
                self.clusters = MeanShift(
                    bandwidth=self.settings.bw, seeds=seeds).fit(distances)

            except ValueError:
                pass

            else:
                self.centerPoints = self.clusters.cluster_centers_

        return self.centerPoints
