from detection import Centroid
import uuid
import numpy as np
import scipy.spatial.distance as sd
from collections import deque


class Track():
    def __init__(self, centroid, confirmCounterMin=3, lostCounterMax=3, maxDistance=3, positioningAverages=3):
        self.uuid = str(uuid.uuid1())

        self.history = deque(maxlen=positioningAverages)
        self.centroid = centroid
        self.confirmCounterMin = confirmCounterMin
        self.lostCounterMax = lostCounterMax
        self.maxDistance = maxDistance

        self.lostCounter = 0
        self.confirmCounter = 0
        self.confirmed = False

    @property
    def meanVelocity(self):
        return np.mean(np.diff(np.array(self.history), axis=0))

    @property
    def centroid(self):
        return self._centroid

    @property
    def avgPosition(self):
        return np.mean(np.array(self.history), axis=0)

    @centroid.setter
    def centroid(self, new_centroid):
        self._centroid = new_centroid
        self.history.append(self.position)

    @property
    def position(self):
        return self.centroid.position

    def found(self):
        self.lostCounter = 0
        self.confirmCounter += 1

        if self.confirmCounter >= self.confirmCounterMin:
            self.confirmed = True

    def lost(self):
        self.lostCounter += 1

        if self.lostCounter >= self.lostCounterMax:
            counterExceeded = True

        else:
            counterExceeded = False

        return counterExceeded


class Tracker():
    def __init__(self):
        self.tracks = []

    def __getConfirmedElements__(self, elements):
        confirmedElements = []

        for element in elements:
            if element.confirmed:
                confirmedElements.append(element)

        return confirmedElements

    def __getPositions__(self, elements, average=False):
        positions = []

        for element in elements:
            if average:
                positions.append(element.avgPosition)

            else:
                positions.append(element.position)

        return np.array(positions)

    def __getConfirmedPositions__(self, elements, average=False):
        return self.__getPositions__(self.__getConfirmedElements__(elements), average)

    @property
    def trackPositions(self):
        return self.__getPositions__(self.tracks)

    @property
    def confirmedTrackPositions(self):
        return self.__getConfirmedPositions__(self.tracks)

    @property
    def averageConfirmedTrackPositions(self):
        return self.__getConfirmedPositions__(self.tracks, average=True)

    @property
    def confirmedTracks(self):
        return self.__getConfirmedElements__(self.tracks)

    @property
    def centroidPositions(self):
        return self.__getPositions__(self.centroids)

    def __initializeTracks__(self):
        # If there are no tracks yet, all centroids are collected as new tracks.
        for centroid in self.centroids:
            self.__newTrack__(centroid)

    def __matchTracks__(self):
        self.matchedTracks = []
        self.matchedCentroids = []

        if self.centroids:
            # Calculate distances between registered tracks and new centroids
            distanceMap = sd.cdist(self.trackPositions, self.centroidPositions)

            trackDistanceOrder = distanceMap.min(axis=1).argsort()
            centroidDistanceOrder = distanceMap.argmin(axis=1)[
                trackDistanceOrder]

            for trackIndex, centroidIndex in zip(trackDistanceOrder, centroidDistanceOrder):
                t = self.tracks[trackIndex]
                c = self.centroids[centroidIndex]

                if t not in self.matchedTracks and c not in self.matchedCentroids:
                    if distanceMap[trackIndex, centroidIndex] <= t.maxDistance:
                        t.centroid = c

                        self.matchedTracks.append(t)
                        self.matchedCentroids.append(c)

                        t.found()

    def __handleLostTracks__(self):
        for t in self.tracks:
            if t not in self.matchedTracks:
                if t.lost():
                    self.__unregister__(t)

    def __handleUnmatchedTracks__(self):
        for c in self.centroids:
            if c not in self.matchedCentroids:
                self.__newTrack__(c)

    def __generateCentroids__(self, points):
        self.centroids = []

        if points is not None:
            for p in points:
                self.centroids.append(Centroid(p[0], p[1]))

    def run(self, centerPoints):

        self.__generateCentroids__(centerPoints)

        if not self.tracks:
            # No tracks exist yet
            self.__initializeTracks__()

        else:
            # Follow-up
            self.__matchTracks__()
            self.__handleLostTracks__()
            self.__handleUnmatchedTracks__()

        return self.confirmedTrackPositions

    def __newTrack__(self, centroid):
        track = Track(centroid)
        self.__register__(track)

    def __register__(self, track):
        self.tracks.append(track)

    def __unregister__(self, track):
        self.tracks.remove(track)
        del track
