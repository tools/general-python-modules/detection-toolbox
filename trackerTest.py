from detection import Track, Tracker
from detection import Detector, DetectorSettings, Centroid
import matplotlib.pyplot as plt
import numpy as np

measurements = [[Centroid(0, 0, 1, 1, 1), Centroid(6, 1, 1, 1, 1),
                 Centroid(1.2, 1.1, 1, 1, 1), Centroid(6.2, 1.4, 1, 1, 1)],
                [Centroid(-0.3, 0.2, 1, 1, 1), Centroid(6.5, 2.5, 1, 1, 1)],
                [Centroid(-0.2, 0.3, 1, 1, 1), Centroid(7.1, 1.5, 1, 1, 1)],
                [Centroid(-0.1, 0.4, 1, 1, 1), Centroid(7.8, 1, 1, 1, 1)],
                [Centroid(0, 0.5, 1, 1, 1), Centroid(8, 0, 1, 1, 1)],
                [Centroid(0.2, 0.8, 1, 1, 1), Centroid(9, -0.5, 1, 1, 1)],
                [Centroid(0.4, 0.8, 1, 1, 1), Centroid(9, -1, 1, 1, 1)],
                [Centroid(0.5, 0.6, 1, 1, 1), Centroid(8, -2, 1, 1, 1)],
                [Centroid(0.8, 0.9, 1, 1, 1), Centroid(9.5, -1, 1, 1, 1)],
                [Centroid(1, 0.8, 1, 1, 1), Centroid(8, -1, 1, 1, 1)],
                [Centroid(1.2, 0.7, 1, 1, 1), Centroid(7, -0.5, 1, 1, 1)],
                [Centroid(1.3, 0.7, 1, 1, 1), Centroid(9, 0, 1, 1, 1)],
                [Centroid(1.4, 0.6, 1, 1, 1), Centroid(8, .5, 1, 1, 1)],
                [Centroid(1.7, 0.5, 1, 1, 1), Centroid(7, 1.5, 1, 1, 1)]]

t = Tracker()

for m in measurements:
    t.run(m)

plt.figure(1)
for track in t.tracks:
    plt.scatter(track.position[0], track.position[1])

    posHistory = np.array(track.history).transpose()
    plt.plot(posHistory[0], posHistory[1])

plt.show()
