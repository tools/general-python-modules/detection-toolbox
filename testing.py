import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from detection import Detector, DetectorSettings

detectorSettings = DetectorSettings()
detector = Detector()

data = cv2.imread("testbild.png", flags=cv2.IMREAD_GRAYSCALE)
cImage = data.copy()

detector.run(data)

cv2.imshow("data", data)

for c in detector.centers:
    cv2.circle(cImage, (int(c.xMid), int(c.yMid)),
               10, (255, 0, 0), 3)

cv2.imshow("targets", cImage)

cv2.waitKey(0)
